<?php

namespace paml\Auth\Google\Factory;

use Interop\Container\ContainerInterface;
use paml\Auth\Google\Helper\AuthGoogleUrlHelper;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthGoogleUrlHelperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new AuthGoogleUrlHelper(
            $container->get('ViewHelperManager')->get('Url')
        );
    }
}
