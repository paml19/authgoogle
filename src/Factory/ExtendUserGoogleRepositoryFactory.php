<?php

namespace paml\Auth\Google\Factory;

use Interop\Container\ContainerInterface;
use paml\Auth\Factory\Repository\AbstractExtendUserRepository;
use paml\Auth\Google\Entity\User;
use paml\Auth\Google\Repository\ExtendedUserRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class ExtendUserGoogleRepositoryFactory
    extends AbstractExtendUserRepository
    implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): ExtendedUserRepository {
        $invoke = parent::__invoke($container, $requestedName, $options);

        return new ExtendedUserRepository(
            $invoke['entityManager'],
            $invoke['entityManager']->getClassMetadata(User::class),
            $invoke['config']['auth']['default_role'],
            $invoke['modules']
        );
    }
}
