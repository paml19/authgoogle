<?php

namespace paml\Auth\Google\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use League\OAuth2\Client\Provider\Google;
use paml\Auth\Entity\User as BaseUser;
use paml\Auth\Google\Entity\User;
use paml\Auth\Google\Repository\ExtendedUserRepository;
use paml\Auth\Google\Service\AuthGoogleService;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthGoogleServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        if (! isset($container->get('Config')['auth']['google'])) {
            throw new \Exception('No google implemented in config!');
        }

        $config = $container->get('Config')['auth']['google'];

        $google = new Google([
            'clientId' => $config['client_id'],
            'clientSecret' => $config['client_secret'],
            'redirectUri' => $config['protocol'] . '://' . $_SERVER['HTTP_HOST'] . $config['redirect_uri'],
            'accessType' => $config['access_type']
        ]);

        return new AuthGoogleService(
            $google,
            $container->get('Route\Session'),
            $container->get(EntityManager::class)->getRepository(BaseUser::class),
            $container->get(EntityManager::class)->getRepository(User::class),
            $container->get(ExtendedUserRepository::class)
        );
    }
}
