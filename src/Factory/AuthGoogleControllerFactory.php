<?php

namespace paml\Auth\Google\Factory;

use Interop\Container\ContainerInterface;
use paml\Auth\Google\Controller\AuthGoogleController;
use paml\Auth\Google\Service\AuthGoogleAdapter;
use paml\Auth\Google\Service\AuthGoogleService;
use paml\Auth\Service\AuthManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthGoogleControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new AuthGoogleController(
            $container->get(AuthGoogleService::class),
            $container->get(AuthManager::class),
            $container->get(AuthGoogleAdapter::class),
            $container->get('Route\Session')
        );
    }
}
