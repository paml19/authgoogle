<?php

namespace paml\Auth\Google\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\View\Helper\Url;

class AuthGoogleUrlHelper extends AbstractHelper
{
    private $url;

    public function __construct(Url $url)
    {
        $this->url = $url;
    }

    public function __invoke()
    {
        return $this->url->__invoke('auth/google', ['action' => 'create']);
    }
}
