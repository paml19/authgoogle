<?php

namespace paml\Auth\Google\Repository;

use paml\Auth\Google\Entity\User;
use paml\Auth\Repository\ExtendedUserRepository as BaseExtendUserRepository;

class ExtendedUserRepository extends BaseExtendUserRepository
{
    public function saveGoogleWithDefaultRole(User $userGoogle): void
    {
        if (! $userGoogle->getId()) {
            $this->saveWithDefaultRole($userGoogle->getUser());
        }

        $this->getEntityManager()->persist($userGoogle);
        $this->getEntityManager()->flush($userGoogle);
    }
}
