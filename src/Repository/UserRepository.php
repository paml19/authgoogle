<?php

namespace paml\Auth\Google\Repository;

use paml\Auth\Repository\UserRepository as BaseUserRepository;

class UserRepository extends BaseUserRepository
{
    public function findByUserEmail(string $email): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('gu')
            ->from($this->getEntityName(), 'gu')
            ->join('gu.user', 'u')
            ->where($queryBuilder->expr()->eq('u.email', ':email'))
            ->setParameter(':email', $email);

        return $queryBuilder->getQuery()->getResult();
    }
}
