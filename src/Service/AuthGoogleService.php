<?php

namespace paml\Auth\Google\Service;

use League\OAuth2\Client\Provider\Google;
use paml\Auth\Entity\User as BaseUser;
use paml\Auth\Form\Constants\Register;
use paml\Auth\Google\Entity\AccessToken;
use paml\Auth\Google\Entity\User;
use paml\Auth\Google\Repository\ExtendedUserRepository;
use paml\Auth\Google\Repository\UserRepository;
use paml\Auth\Listener\HistoryListener;
use paml\Auth\Repository\UserRepository as BaseUserRepository;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Session\Container;
use League\OAuth2\Client\Token\AccessToken as OAuthAccessToken;

class AuthGoogleService
{
    private $google;

    private $session;

    private $extendedUserRepository;

    private $baseUserRepository;

    private $userRepository;

    public function __construct(
        Google $google,
        Container $session,
        BaseUserRepository $baseUserRepository,
        UserRepository $userRepository,
        ExtendedUserRepository $extendedUserRepository
    ) {
        $this->google = $google;
        $this->session = $session;
        $this->extendedUserRepository = $extendedUserRepository;
        $this->baseUserRepository = $baseUserRepository;
        $this->userRepository = $userRepository;
    }

    public function createAuth(): void
    {
        $authUrl = $this->google->getAuthorizationUrl();
        $this->session->oauth2state = $this->google->getState();
        header('Location: ' . $authUrl);exit;
    }

    public function handleRequest(Params $requestParams): string
    {
        $error = $requestParams->fromQuery('error', null);
        $state = $requestParams->fromQuery('state', null);
        $code = $requestParams->fromQuery('code', null);

        if ($error) {
            throw new \Exception('Error: ' . htmlspecialchars($error, ENT_QUOTES, 'UTF-8'));
        }

        if (! $state || empty($state) || ($state !== $this->session->oauth2state)) {
            unset($this->session->oauth2state);
            throw new \Exception('Invalid state');
        }

        return $code;
    }

    public function getAccessToken(string $code): OAuthAccessToken
    {
        $token = $this->google->getAccessToken('authorization_code', [
            'code' => $code,
        ]);

        return $token;
    }

    public function createUser(OAuthAccessToken $token): UserAndHistory
    {
        try {
            $ownerDetails = $this->google->getResourceOwner($token);
            $formData = [
                Register::NAME => $ownerDetails->getFirstName(),
                Register::SURNAME => $ownerDetails->getLastName(),
                Register::EMAIL => $ownerDetails->getEmail()
            ];

            $historyType = null;

            $baseUser = $this->baseUserRepository->findOneBy(['email' => $formData[Register::EMAIL]]);

            if (! $baseUser) {
                $baseUser = (new BaseUser())
                    ->setName($formData[Register::NAME])
                    ->setSurname($formData[Register::SURNAME])
                    ->setEmail($formData[Register::EMAIL]);

                $historyType = HistoryListener::HISTORY_TYPE_REGISTER;
            }

            $baseUser->setActive(true);

            $user = current($this->userRepository->findByUserEmail($formData[Register::EMAIL]));

            if (! $user) {
                $user = (new User())
                    ->setUser($baseUser);
            }

            $accessToken = (new AccessToken())
                ->setAccessToken($token->getToken())
                ->setDateExpire($token->getExpires());

            $user->addAccessToken($accessToken);

            $this->extendedUserRepository->saveGoogleWithDefaultRole($user);

            return (new UserAndHistory($user, $historyType));
        } catch (\Exception $e) {
            throw new $e;
        }
    }
}
