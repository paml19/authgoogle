<?php

namespace paml\Auth\Google\Service;

use paml\Auth\Entity\User;
use paml\Auth\Repository\UserRepository;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

class AuthGoogleAdapter implements AdapterInterface
{
    private $login;

    private $password;

    private $userRepository;

    private $userId;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function authenticate()
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['login' => $this->login]);

        if (! $user) {
            $user = $this->userRepository->findOneBy(['email' => $this->login]);
        }

        if (! $user) {
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null, [
                'Identity not found'
            ]);
        }

        if (! $user->isActive()) {
            return new Result(Result::FAILURE, null, [
                'Status inactive'
            ]);
        }

        return new Result(Result::SUCCESS, $user->toAuthArray(), [
            'Authenticated successfully'
        ]);
    }

    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setUserId(?string $userId)
    {
        $this->userId = $userId;
        return $this;
    }
}
