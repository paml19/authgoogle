<?php

namespace paml\Auth\Google\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use paml\Auth\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="paml\Auth\Google\Repository\UserRepository")
 * @ORM\Table(name="auth_google_user")
 * @GEDMO\Loggable
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=false)
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="date_edit", type="datetime", nullable=true)
     */
    private $dateEdit;

    /**
     * @ORM\Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @ORM\OneToOne(targetEntity="\paml\Auth\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(
     *     targetEntity="\paml\Auth\Google\Entity\AccessToken",
     *     mappedBy="user",
     *     cascade={"persist", "remove"}
     *     )
     */
    private $accessTokens;

    public function __construct()
    {
        $this->accessTokens = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getUser(): ?BaseUser
    {
        return $this->user;
    }

    public function setUser(BaseUser $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getDateAdd(): \DateTime
    {
        return $this->dateAdd;
    }

    public function getDateEdit(): ?\DateTime
    {
        return $this->dateEdit;
    }

    public function getDateDelete(): ?\DateTime
    {
        return $this->dateDelete;
    }

    public function getAccessTokens(): Collection
    {
        return $this->accessTokens;
    }

    public function setAccessTokens(Collection $accessTokens)
    {
        $this->accessTokens = $accessTokens;
        return $this;
    }

    public function addAccessToken(AccessToken $accessToken): self
    {
        if ($this->accessTokens->contains($accessToken)) {
            return $this;
        }

        $accessToken->setUser($this);
        $this->accessTokens->add($accessToken);
        return $this;
    }

    public function removeAccessToken(AccessToken $accessToken): self
    {
        if (! $this->accessTokens->contains($accessToken)) {
            return $this;
        }

        $this->accessTokens->remove($accessToken);
        return $this;
    }
}
