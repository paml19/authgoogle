<?php

namespace paml\Auth\Google\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="paml\Auth\Google\Repository\AccessTokenRepository")
 * @ORM\Table(name="auth_google_access_token")
 * @GEDMO\Loggable
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=false)
 */
class AccessToken
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @ORM\ManyToOne(targetEntity="\paml\Auth\Google\Entity\User", inversedBy="accessTokens")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(name="accessToken", type="string", length=200, unique=true, nullable=false)
     */
    private $accessToken;

    /**
     * @ORM\Column(name="date_expire", type="datetime", nullable=false)
     */
    private $dateExpire;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getDateAdd(): \DateTime
    {
        return $this->dateAdd;
    }

    public function getDateDelete(): ?\DateTime
    {
        return $this->dateDelete;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    public function setAccessToken(string $accessToken): self
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    public function getDateExpire(): \DateTime
    {
        return $this->dateExpire;
    }

    public function setDateExpire(int $dateExpire): self
    {
        $this->dateExpire = \DateTime::createFromFormat('U', $dateExpire);
        return $this;
    }
}
