<?php

namespace paml\Auth\Google\Controller;

use paml\Auth\Exception\AuthorizationException;
use paml\Auth\Google\Service\AuthGoogleAdapter;
use paml\Auth\Google\Service\AuthGoogleService;
use paml\Auth\Google\Service\UserAndHistory;
use paml\Auth\Listener\HistoryListener;
use paml\Auth\Module;
use paml\Auth\Service\AuthManager;
use Zend\Authentication\Result;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class AuthGoogleController extends AbstractActionController
{
    private $authGoogleService;

    private $authManager;

    private $authGoogleAdapter;

    private $sessionContainer;

    public function __construct(
        AuthGoogleService $authGoogleService,
        AuthManager $authManager,
        AuthGoogleAdapter $authGoogleAdapter,
        Container $sessionContainer
    ) {
        $this->authGoogleService = $authGoogleService;
        $this->authManager = $authManager;
        $this->authGoogleAdapter = $authGoogleAdapter;
        $this->sessionContainer = $sessionContainer;
    }

    public function indexAction()
    {
        return new ViewModel();
    }

    public function createAction()
    {
        $this->authGoogleService->createAuth();
    }

    public function callbackAction()
    {
        $code = $this->authGoogleService->handleRequest($this->params());
        $oAuthAccessToken = $this->authGoogleService->getAccessToken($code, $this->getEventManager());
        $userAndHistory = $this->authGoogleService->createUser($oAuthAccessToken);
        return $this->login($userAndHistory);
    }

    private function login(UserAndHistory $userAndHistory)
    {
        $user = $userAndHistory->getUser();
        $historyType = $userAndHistory->getHistoryType();

        if ($historyType) {
            $this->getEventManager()->trigger(
                Module::AUTH_USER_HISTORY,
                $this,
                [
                    'userId' => $user->getUser()->getId(),
                    'type' => $historyType,
                    'success' => true,
                ]
            );

            $this->getEventManager()->trigger(
                Module::AUTH_USER_REGISTERED_IN,
                $this,
                ['user' => $user->getUser()]
            );
        }

        $result = $this->authManager->login(
            $user->getUser()->getEmail(),
            '',
            true,
            $this->authGoogleAdapter
        );

        if ($result->getCode() == Result::SUCCESS) {
            $this->getEventManager()->trigger(
                Module::AUTH_USER_HISTORY,
                $this,
                [
                    'userId' => $this->authManager->getIdentity()['id'],
                    'type' => HistoryListener::HISTORY_TYPE_LOGIN,
                    'success' => true,
                ]
            );

            if (isset($this->sessionContainer->route)) {
                return $this->redirect()->toUrl($this->sessionContainer->route);
            }

            return $this->redirect()->toRoute('home');
        } else {
            $this->getEventManager()->trigger(
                Module::AUTH_USER_HISTORY,
                $this,
                [
                    'userLogin' => $user->getUser()->getEmail(),
                    'type' => HistoryListener::HISTORY_TYPE_LOGIN
                ]
            );

            throw new AuthorizationException('Not logged, result: ' . $result->getMessages()[$result->getCode()], 401);
        }
    }
}
