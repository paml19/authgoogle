<?php
namespace paml\Auth\Google;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'controllers' => [
        'factories' => [
            Controller\AuthGoogleController::class => Factory\AuthGoogleControllerFactory::class
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    'router' => [
        'routes' => [
            'auth' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/auth',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'google' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/google[/:action]',
                            'defaults' => [
                                'controller' => Controller\AuthGoogleController::class,
                                'action' => 'index',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Repository\ExtendedUserRepository::class => Factory\ExtendUserGoogleRepositoryFactory::class,
            Service\AuthGoogleService::class => Factory\AuthGoogleServiceFactory::class,
            Service\AuthGoogleAdapter::class => \paml\Auth\Factory\Service\AuthAdapterFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            Helper\AuthGoogleUrlHelper::class => Factory\AuthGoogleUrlHelperFactory::class,
        ],
        'aliases' => [
            'googleUrl' => Helper\AuthGoogleUrlHelper::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'paml\Auth\Google' => __DIR__ . '/../view',
        ],
    ],
];
